import { fetchFact } from "../../api/factApi";
import { useQuery } from "react-query";
import styles from "./notification.module.scss";
import * as classNames from "classnames";

export const Notification = () => {
  const { data, isLoading, error } = useQuery("fact", fetchFact);

  return (
    <div className={classNames(styles.root, { [styles.isInvalid]: !!error })}>
      <>
        {isLoading && "Loading..."}
        {data && !isLoading && data.text}
        {error && `Error: ${error.message}`}
      </>
    </div>
  );
};
