# OAK'S LAB | Audition Assignment
## DOD
- Create a Todo list in React with stored progress in the localStorage

## Used technologies
- **Vite** - bundler
- **React** 
- **Typescript**
- **React-query** - data fetching
- **SCSS modules** - styling
- **Prettier** for formatting


## How to run project
1. Clone repository
2. Go to the project root
3. run `yarn` for install dependencies
4. run `yarn dev` for run project