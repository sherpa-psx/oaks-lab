import { Todo } from "./todo.model";

export default interface TodoGroup {
  id: string;
  title: string;
  todos: Todo[];
}
