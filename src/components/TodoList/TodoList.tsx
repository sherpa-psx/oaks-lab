import { useCallback, useEffect, useMemo, useState } from "react";
import TodoGroupInterface from "../../models/group.model";
import { initialData } from "../../initialData";
import { TodoGroup } from "../TodoGroup/TodoGroup";
import { Notification } from "../Notification/Notification";
import styles from "./todoList.module.scss";

const TodoList = () => {
  const [todoGroups, setTodoGroups] =
    useState<TodoGroupInterface[]>(initialData);
  const [isInitialMount, setIsInitialMount] = useState(true);

  const allTodosAreDone = useMemo(
    () =>
      todoGroups.flatMap((group) => group.todos).every((todo) => todo?.checked),
    [todoGroups]
  );

  // Function to handle todo state change
  const handleTodoChange = (groupId: string, todoId: string) => {
    setTodoGroups((prevTodoGroups) => {
      return prevTodoGroups.map((group) => {
        if (group.id === groupId) {
          const updatedTodos = group.todos?.map((todo) =>
            todo.id === todoId ? { ...todo, checked: !todo.checked } : todo
          );
          return { ...group, todos: updatedTodos };
        }
        return group;
      });
    });
  };

  // Load todo states from local storage (or set to) on component mount
  useEffect(() => {
    const storedGroups = JSON.parse(
      localStorage.getItem("todoGroups") || "[]"
    ) as TodoGroupInterface[];

    if (storedGroups.length === 0) {
      localStorage.setItem("todoGroups", JSON.stringify(todoGroups));
    } else {
      setTodoGroups(storedGroups);
    }
  }, []);

  // Update local storage when todo states change
  useEffect(() => {
    if (!isInitialMount) {
      localStorage.setItem("todoGroups", JSON.stringify(todoGroups));
    } else {
      setIsInitialMount(false);
    }
  }, [todoGroups, isInitialMount]);

  // Handle if group is disabled
  const isGroupDisabled = useCallback(
    (groupIndex: number) => {
      if (groupIndex === 0) return false;
      if (todoGroups.length === 1) return false;
      const flattenGroups = todoGroups
        .slice(0, groupIndex)
        .flatMap((group) => group.todos);

      return !flattenGroups.every((todo) => todo.checked);
    },
    [todoGroups]
  );

  return (
    <div className={styles.root}>
      {todoGroups.map((group: TodoGroupInterface, i: number) => (
        <TodoGroup
          {...group}
          key={group.id}
          order={i + 1}
          isLocked={isGroupDisabled(i)}
          onChange={handleTodoChange}
          isDone={group.todos.every((todo) => todo.checked)}
        />
      ))}
      {allTodosAreDone && <Notification />}
    </div>
  );
};

export default TodoList;
