import TodoGroup from "./models/group.model";

export const initialData: TodoGroup[] = [
  {
    id: "1",
    title: "Foundation",
    todos: [
      { id: "1", title: "Set virtual office", checked: false },
      { id: "2", title: "Set mission", checked: false },
      { id: "3", title: "Buy domains", checked: false }
    ]
  },
  {
    id: "2",
    title: "Discovery",
    todos: [
      { id: "4", title: "Create roadmap", checked: false },
      { id: "5", title: "Competitor analysis", checked: false }
    ]
  },
  {
    id: "3",
    title: "Delivery",
    todos: [
      { id: "6", title: "Release marketing website", checked: false },
      { id: "7", title: "Release MVP", checked: false }
    ]
  }
];
