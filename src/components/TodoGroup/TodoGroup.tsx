import GroupInterface from "../../models/group.model";
import styles from "./todoGroup.module.scss";
import classnames from "classnames";

interface Props extends GroupInterface {
  isLocked: boolean;
  isDone: boolean;
  order: number;
  onChange: (groupId: string, checkboxId: string) => void;
}

export const TodoGroup = ({
  id,
  title,
  todos,
  isLocked,
  isDone,
  order,
  onChange
}: Props) => {
  return (
    <div key={id} className={classnames({ [styles.isLocked]: isLocked })}>
      <h4 className={styles.title}>
        <span className={styles.order}>{order}</span>
        {title}
        {isDone && <span className={styles.checkMark} />}
      </h4>
      {todos?.map((todo) => {
        return (
          <div key={todo.id}>
            <input
              type="checkbox"
              id={todo.id}
              checked={todo.checked}
              disabled={isLocked}
              onChange={() => onChange(id, todo.id)}
            />
            <label htmlFor={todo.id}>{todo.title}</label>
          </div>
        );
      })}
    </div>
  );
};
