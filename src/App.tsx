import TodoList from "./components/TodoList/TodoList";
import { QueryClient, QueryClientProvider } from "react-query";
import "./styles.scss";
const queryClient = new QueryClient();

export default function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <div className="App">
        <TodoList />
      </div>
    </QueryClientProvider>
  );
}
